# Example test CI pipeline/job:

The `.gitlab-ci.yml` file in this repo builds a simple Docker image. This is used to test a gitlab-runner to ensure that the runner is configured correctly. i.e. TO handle docker-image-build jobs in a pipeline, the runner would need to be configured with certain configuration options required for building images. .i.e. The `config.toml`  for a gitlab runner primarily will look like this:

```
[[runners]]
  url = "https://gitlab.com/"
  token = TOKEN
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "alpine"
    privileged = true
    disable_cache = false
    volumes = ["/certs/client", "/cache"]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```

The most important lines (for building docker image) are:
```
    tls_verify = false
    privileged = true
    volumes = ["/certs/client", "/cache"]
```

A (simplified) job to actually build a docker image inside `.gitlab-ci.yml` file will look like this:

```
build-test-docker-image:
  stage: build-docker-image
  only:
    - master
    - main
  image: docker:latest
  services:
    - docker:dind
  tags:
    - kbinfra-server1
    - waqar-home
  script:
    # Docker-image-name cannot have dots in it, so replace dots with dash.
    - DOCKER-IMAGE_NAME=$(echo ${CI_PROJECT_NAME} | tr '.' '-')
    - |
      docker build \
        -t test/${DOCKER-IMAGE_NAME}:${CI_COMMIT_SHORT_SHA} \
        -t test/${DOCKER-IMAGE_NAME}:latest .
```

The `tags` are optional. These (tags) help select which runner to choose for this particular job.
